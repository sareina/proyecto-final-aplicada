from ui import(
menu,                       #llamamos las funciones de ui.py, para ejecutarlas aqui
nuevo_proyecto,
listado_proyectos,
ver_proyecto,
user_story,
ver_user,
editar_user,
finalizar_user
)

from Datos_Programa import(     #llamamos las funciones de Datos_proyectos, no se llaman todas
crear_proyecto,                 #Solo se definen las de crear proyecto y user story porque las demas funciones ya son llamadas en ui.py
crear_user

)

CREACION_PROYECTO   = '1'      #Definimos con que valor se van a llamar las funciones para cada parametro
LISTADOS_PROYECTOS= '2'        #Creamos nuevas funciones y las llamamos segun la opcion que escojamos 
CONSULTAR_PROYECTOS ='3'
CREACION_USER= '4'
CONSULTAR_USER= '5'
EDICION_USER= '6'
ADIOS_USER= '7'
SALIR = '8'

opcion = ''
while opcion != SALIR:        #Mientras no sea la opcion de salir se ejecuta el programa
    # Pedir al usuario la opcion
    opcion = menu()           #Siempre llama al menu automaticamente
    # Verifica la opcion 
    if opcion == CREACION_PROYECTO:
        # Pide los datos al usuario para crear el proyecto
        nombre_proyecto, descripcion = nuevo_proyecto()
        # Verifica los datos en la base de datos
        crear_proyecto( nombre_proyecto, descripcion)
    elif opcion == LISTADOS_PROYECTOS:
        # Muestra los datos almacenados en la base de datos, no es necesario ingresar datos aqui 
        listado_proyectos()
    elif opcion == CONSULTAR_PROYECTOS:
        # Muestra los datos almacenados en la base de datos, no es necesario ingresar datos aqui 
        ver_proyecto()
    elif opcion == CREACION_USER:
         # Pide los datos al usuario para crear el user story
        id_proyecto, codigo_user, nombre_user, card, conversation, confirmation = user_story()
         # Verifica los datos en la base de datos
        crear_user(id_proyecto, codigo_user, nombre_user, card, conversation, confirmation)
    elif opcion == CONSULTAR_USER:
         # Muestra los datos almacenados en la base de datos, no es necesario ingresar datos aqui 
        ver_user()

    elif opcion == EDICION_USER:
        # Muestra los datos almacenados en la bd
        editar_user()

    elif opcion == ADIOS_USER:
         # Muestra los datos almacenados en la bd
        finalizar_user()
    
    elif opcion == SALIR:
        print('Muchas gracias, vuelva pronto')
    else:
        print('la opcion no es valida')