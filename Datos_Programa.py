import psycopg2
from Datos_Conexion import dc                                           #llamamos a la funcion dc desde datos conexion


def crear_proyecto(nombre, descripcion):                                #Creamos la funcion para crear proyecto y llamamos alli ciertas variables
    conexion = psycopg2.connect(**dc)                                   #obtener conexion con la base de datos creada
    cursor = conexion.cursor()                                          #obtener el cursor, se realiza para cada una de las funciones

    #requerimiento de creacion del proyecto
    sql = 'insert into proyecto (nombre, descripcion) values (%s, %s)'  #insertamos en la tabla proyecto dentro de las columnas respectivas los valores a asignar
    parametros= (nombre, descripcion)                                   #Definimos los parametros que se van a agregar 
    cursor.execute(sql, parametros)                                     #Ejecutamos la cadena en la base de datos
    conexion.commit()                                                   #Guardar los datos en la base de datos
    print(f'\n\t El proyecto {nombre} fue creado satisfactoriamente'    #Nos sale mensaje con los parametros agregados
         f'\n Descripcion{descripcion}'                                 #Se vera luego de ejecutar el programa en la base de datos
         )
    #Cerrar el cursor y la conexion con la base de datos
    cursor.close()
    conexion.close()


def lista_proyectos():
    #obtener conexion a la base de datos
    conexion = psycopg2.connect(**dc)
    #obtener el cursor
    cursor = conexion.cursor()
    print('\t LISTA DE PROYECTOS')
    #Seleccionar proyecto
    cursor.execute('select * from proyecto')
    #obtener todas las filas
    filas =cursor.fetchall()

    if( filas == None ):
        print('-----------------------')
        print('\nNo se encuentra ningun proyecto')
        return

    #recorrer las filas
    for fila in filas:
        nombre = fila[1]
        descripcion = fila[2]
        print('-------------------------------------')
        print(f'\t nombre={nombre}\t descripcion={descripcion}')
    
    #Cerrar el cursor y la conexion
    cursor.close()
    conexion.close()

def mirar_proyecto(codigo):
    #obtener conexion a la base de datos
    conexion = psycopg2.connect(**dc)
    #obtener el cursor
    cursor = conexion.cursor()
    print('\tProyecto')
    #Seleccionar proyecto
    sql= 'select proyecto.nombre,userstory.codigo,userstory.nombre from proyecto, userstory where proyecto.id=%s and userstory.idproyecto=%s'
    parametros = ( codigo, codigo )
    cursor.execute(sql,parametros)
    filas =cursor.fetchall()
    if( len(filas) == 0 ):
        print('-----------------------')
        print('\nNo se encuentra ningun proyecto')
        return
    for fila in filas:
            nombre= fila[0]
            codigo_user= fila[1]
            nombre_user= fila[2]
            print('-------------------------------------')
            print(f'nombre={nombre}\t codigo_user={codigo_user}\t nombre_user={nombre_user}')
    else:
           print('-------------------------------------------')
           print('\nEl proyecto se encontro satisfactoriamente.')
    
    
    # Cierra el cursor y la conexion
    cursor.close()
    conexion.close() 

def crear_user(id_proyecto, codigo_user, nombre_user, card, conversation, confirmation):
    #obtener conexion a la base de datos
    conexion = psycopg2.connect(**dc)
    #obtener el cursor
    cursor = conexion.cursor()
    #creamos el user story
    sql = 'insert into userstory (idproyecto, codigo, nombre, card, conversation, confirmation) values (%s, %s, %s, %s, %s, %s) '
    parametros=(id_proyecto, codigo_user, nombre_user, card, conversation, confirmation)
    try:
        cursor.execute(sql,parametros)
        conexion.commit()
    except:
        print('El proyecto no existe, elija otra opcion')
        return
    print(f'\n\t El UserStory {nombre_user} fue creado satisfactoriamente'
         f'\nCodigo: {codigo_user}'
         f'\nCard: {card}'
         f'\nConversation: {conversation}'
         f'\nConfirmation: {confirmation}'
         f'\nCodigo Proyecto: {id_proyecto}'
         )
    # Cierra el cursor y la conexion
    cursor.close()
    conexion.close()

def consultar_user(codigo_user):
    #obtener conexion a la base de datos
    conexion = psycopg2.connect(**dc)
    #obtener el cursor
    cursor = conexion.cursor()
    print('\t CONSULTAR USER STORY')
    #Seleccionar user
    sql='select proyecto.nombre, userstory.codigo, userstory.nombre, userstory.card, userstory.conversation, userstory.confirmation from proyecto, userstory where userstory.codigo=%s and userstory.idproyecto=proyecto.id'
    cursor.execute(sql, (codigo_user,))
    #obtener todas las filas
    filas = cursor.fetchall()
    
    if(len(filas)==0):
        print('-----------------------')
        print('\nNo se encuentra ningun proyecto')
        return
    
    #recorrer las filas
    for fila in filas:
        nombre = fila[0]
        codigo_user = fila[1]
        nombre_user = fila[2]
        card= fila[3]
        conversation =fila[4]
        confirmation = fila[5]
        print('--------------------------')
        print(f'\t nombre del proyecto={nombre} \t codigo del user={codigo_user} \t nombre del user={nombre_user} \t card={card} \t conversation={conversation} \t confirmation={confirmation}')
    
    # Cierra el cursor y la conexion
    cursor.close()
    conexion.close()
  

def cambiar_user(codigo_user, nombre_user, card, conversation,confirmation):
    #obtener conexion a la base de datos
    conexion = psycopg2.connect(**dc)
    #obtener el cursor
    cursor = conexion.cursor()
    print('\t EDITAR USER STORY')
    #Seleccionar proyecto
    sql='update userstory set nombre=%s, card=%s, conversation=%s, confirmation=%s where codigo=%s'
    parametros=(nombre_user,card,conversation,confirmation, codigo_user)
    cursor.execute(sql, parametros)
    conexion.commit()
    print('Los datos se cambiaron en este user story')
    
    #Cerrar el cursor y la conexion
    cursor.close()
    conexion.close()

def borrar_user(codigo_user):
    #obtener conexion a la base de datos
    conexion = psycopg2.connect(**dc)
    #obtener el cursor
    cursor = conexion.cursor()
    print('\t BORRAR USER STORY')
    sql='select nombre from userstory where codigo=%s'
    parametros=(codigo_user,)
    cursor.execute(sql,parametros)
    
    filas = cursor.fetchall()

    if( filas == None ):
        print('-----------------------')
        print('\nNo se encuentra el dato necesario')
        return

    #recorrer las filas
    print(f'Esta seguro que desea borrar el user story {filas[0][0]} ?')
    if( input('S/N:   ') == 'S'):
        sql='delete from userstory where codigo=%s'
        parametros=(codigo_user,)
        cursor.execute(sql,parametros)
        conexion.commit()
        print('\nSe eliminó el userstory')

    #Cerrar el cursor y la conexion
    cursor.close()
    conexion.close()