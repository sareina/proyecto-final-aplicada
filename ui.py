
## Menu del programa
def menu():
    print('\n\n Bienvenido al programa SCRUM')   #El menu siempre se vera reflejado al iniciar el programa
    print('1. Crear Proyecto')                   #Todas las opciones se van dando acorde a los requerimientos
    print('2. Consultar Lista De Proyectos')     #Son 7 los requerimientos hasta la ultima opcion donde se saldra del programa
    print('3. Consultar Un Proyecto')            #El proyecto va referenciado con relacion a el metodo SCRUM
    print('4. Crear User Story')
    print('5. Consultar User Story')
    print('6. Editar User Story')
    print('7. Eliminar User Story')
    print('8. Salir del menu')
    opcion= input('Escoger la opcion que se desea...')
    return opcion                               #Retorna segun la opcion que escogamos


##Obtenemos los datos para la creacion del proyecto

def nuevo_proyecto():     #creamos la funcion para "nuevo proyecto"
    print('\t 1. Bienvenido a la creacion de un nuevo proyecto')
    print('\n Para crear un proyecto por favor ingrese la siguiente informacion ')
    print('\n los campos con * son obligatorios')     #asignamos los campos de mensajes que se veran al dar la opcion

    nombre_proyecto= input('Nombre*:')   #la variable nombre_proyecto se instanciara para dar el nombre al proyecto
    if len(nombre_proyecto)>0 and len(nombre_proyecto)<=200:    #Dar limite al nombre del proyecto, no se continua si es erroneo
        descripcion=input('Descripcion:')   #Variable descripcion para dar descripcion al proyecto
        if len(descripcion)>0 and len(descripcion)<=1000:   #Dar limite a la descripcion del proyecto
            return nombre_proyecto, descripcion     #Devuelve los datos que ingresamos previamente en las variables
        else:
            print('*El nombre del proyecto es muy largo, intentelo de nuevo')
    else:
        print('Error, este campo es obligatorio')   #Mensajes de error en caso que no se cumplan los parametros dados


## Datos para ver la lista de proyectos del programa

def listado_proyectos():
    print('\t 2. Bienvenido a la lista de proyectos')
    from Datos_Programa import(
        lista_proyectos
    )
    lista_proyectos()

##Datos para consultar un proyecto

def ver_proyecto():
    print('\t 3. Bienvenido a la visualización de los proyectos')
    print('selccione el codigo del proyecto que desea ver ')
    cod_proyecto = input()
    from Datos_Programa import(
        mirar_proyecto
    )
    mirar_proyecto(cod_proyecto)

##Datos para la creacion del user story

def user_story():
    print('\t 4. Bienvenido a la creación de un user story')
    print('\n Para crear un user story por favor ingrese la siguiente información:')
    print('\n los campos con * son obligatorios')
    id_proyecto=input('Codigo proyecto*...')
    codigo_user= input('Codigo*...') 
    if len(codigo_user)>0 and len(codigo_user)<=45:
        nombre_user= input('Nombre*...') 
        if len(nombre_user)<=500:
            card=input('Card...')
            if len(card)<=500:
                conversation=input('Conversation...')
                if len(conversation)<=5000:
                    confirmation=input('Confirmation...')
                    if len(confirmation)<=5000:
                        return id_proyecto, codigo_user, nombre_user, card, conversation, confirmation
                    else:
                        print('Error, mensaje muy largo no exceda de 45 dígitos')
                   
                else:
                    print('Error, mensaje muy largo no exceda de 500 dígitos')
                
            else:
                print('Error, mensaje muy largo no exceda de 5000 dígitos')

        else:
            print('Error, mensaje muy largo no exceda de 5000 dígitos')

            
##Datos para consultar los user story

def ver_user():

    print('\t 5. Bienvenido a la visualización de los User story')
    print('Digite el codigo del user story ')
    codigo_user=input()
    from Datos_Programa import(
        consultar_user
    )
    consultar_user(codigo_user)

##Datos para editar los user story


def editar_user():
    print('\t 6. Bienvenido a la edicion del user story')
    codigo_user = input('Digite el codigo del userstory a actualizar: ')
    nombre_user=input('Digite el nombre a actualizar: ')
    card=input('Digite el card a actualizar: ')
    conversation=input('Digite el conversation a actualizar: ')
    confirmation=input('Digite el confirmation a actualizar: ')
    from Datos_Programa import (
        cambiar_user
    )
    cambiar_user(codigo_user, nombre_user, card, conversation, confirmation)

##Datos para finalizar un user story

def finalizar_user():
    print('\t 7. Bienvenido a la eliminacion de los User story')
    print('Digite el nombre del user story que desea finalizar: ')
    codigo_user= input('Codigo: ')
    from Datos_Programa import (
        borrar_user
    )
    borrar_user(codigo_user)
